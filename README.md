# VCO-TH555

VCO using LM555, which approximates the CEM3340. Designed by Thomas Henry.

Taken from [The VCO-555](http://www.electro-music.com/forum/topic-54623.html)

 - v0.1 is the original redrawn schematic by LFlab, see [this post](http://www.electro-music.com/forum/post-366804.html#366804)
 - v0.2 has had ERC errors removed
 - v0.25 Schematic errors fixed
 - v0.26 Reinstated thermistor
 - v0.27.1 Q2/Q3 -> 2SA798
 - v0.27.2 Q2/Q3 -> SSM2220

See also [VCO madness](https://gr33nonline.wordpress.com/2019/08/28/vco-madness/)